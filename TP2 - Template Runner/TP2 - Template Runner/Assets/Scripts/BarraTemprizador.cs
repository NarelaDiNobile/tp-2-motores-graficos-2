using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BarraTemprizador : MonoBehaviour
{

    public static float TiempoOriginal;
    public static float TiempoPasado;
    public static float TiempoRestante;
    public Image BarraTiempo;

    // Start is called before the first frame update
    void Start()
    {
        //hola :D
        TiempoOriginal = 20000;
        TiempoPasado = 0;
        TiempoRestante = TiempoOriginal;
        //BarraTiempo.fillAmount /= 2;
    }

    // Update is called once per frame
    void Update()
    {
        StartCoroutine("Esperar");
        if (TiempoPasado <= TiempoOriginal)
        {
            StartCoroutine("Esperar");
            TiempoPasado += 1;
            TiempoRestante = TiempoOriginal - TiempoPasado;
            BarraTiempo.fillAmount = TiempoRestante / TiempoOriginal;


        }
        else
        {
            Controller_Hud.gameOver = true;
        }
    }
    private IEnumerator Esperar()
    {
        int tiempo_espera = 20;
        while (tiempo_espera > 0)
        {
            yield return new WaitForSeconds(1.0f);
            tiempo_espera--;
        }
    }
}
