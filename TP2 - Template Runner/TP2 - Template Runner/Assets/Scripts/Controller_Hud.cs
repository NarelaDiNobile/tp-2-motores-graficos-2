﻿using System;
using System.Globalization;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class Controller_Hud : MonoBehaviour
{
    public static bool gameOver = false;
    public Text distanceText;
    public Text EnddistanceText;
    //public Text gameOverText;
    private float distance = 0;
    public static int DistanciaRedondeada;
    public TMP_Text EndRecordText;
    public Text EndMonedaText;
    public GameObject EndScreen;
    public AudioSource SonidoFondo;
    

    void Start()
    {
        gameOver = false;
        distance = 0;
        DistanciaRedondeada = Mathf.RoundToInt(distance);
        distanceText.text = DistanciaRedondeada.ToString() + " km";
        //EnddistanceText.text = DistanciaRedondeada.ToString() + " km";
        //gameOverText.gameObject.SetActive(false);
    }

    void Update()
    {
        if (gameOver)
        {
            Time.timeScale = 0;
            EndScreen.gameObject.SetActive(true);
            SonidoFondo.Stop();
            EnddistanceText.text = " " + DistanciaRedondeada + " km";
            EndMonedaText.text = " " + ColeccionControl.ContadorMoneda;
            EndRecordText.text = " " + RecordPlayer.recordkilometro + " km";
            if (Input.GetKey(KeyCode.E))
            {
                Application.Quit();

            }


        }
        else
        {
            distance += Time.deltaTime;
            DistanciaRedondeada = Mathf.RoundToInt(distance);
            distanceText.text = DistanciaRedondeada.ToString() + " km";
        }
    }
}
