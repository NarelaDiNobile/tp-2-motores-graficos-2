using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColeccionMoneda : MonoBehaviour
{
    public AudioSource SonidoMoneda;

    void OnTriggerEnter(Collider other)
    {
        SonidoMoneda.Play();
        ColeccionControl.ContadorMoneda += 1;
        this.gameObject.SetActive(false);
    }

}
