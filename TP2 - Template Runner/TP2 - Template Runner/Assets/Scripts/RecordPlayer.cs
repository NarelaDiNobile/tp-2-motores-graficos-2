using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
//using System.Runtime.Hosting;
using System.IO;
//using System.Diagnostics;

public class RecordPlayer : MonoBehaviour
{
    //private Controller_Hud scriptControllerHud;
    public static int recordkilometro;
    
    

    // Start is called before the first frame update
    void Start()
    {
        
        LeerRecord();
        
        

    }

    // Update is called once per frame
    void Update()
    {
        if (Controller_Hud.gameOver)
        {
            if (Controller_Hud.DistanciaRedondeada > recordkilometro)
            {
                recordkilometro = Controller_Hud.DistanciaRedondeada;
                GrabarRecord();
            }
           
        }
        
    }
    void LeerRecord()
    {
        string filePath = Application.dataPath + "/Record.bin";
        
        using (BinaryReader reader = new BinaryReader(File.Open(filePath, FileMode.Open)))
        {
            recordkilometro = reader.ReadInt32();
        }
        
    }
    void GrabarRecord()
    {
        string filePath = Application.dataPath + "/Record.bin";
        
        using (BinaryWriter writer = new BinaryWriter(File.Open(filePath, FileMode.Create)))
        {
            writer.Write(recordkilometro);
        }

    }

}
