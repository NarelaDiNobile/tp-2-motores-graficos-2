﻿using System.Collections.Generic;
//using System.Diagnostics;
using UnityEngine;

public class Controller_Instantiator : MonoBehaviour
{
    public List<GameObject> enemies;
    public GameObject Gasolina;
    public GameObject instantiatePos;
    public GameObject Moneda;
    public float respawningTimerMoneda;
    public float respawningTimer;
    public float respawningTimerGasolina;
    private float time = 0;

    void Start()
    {
        Controller_Enemy.enemyVelocity = 0.1f;
        ControllerGasolina.gasolinavelocity = 1;
        ControllerMoneda.Monedavelocidad = 1;
    }

    void Update()
    {
        SpawnEnemies();
        ChangeVelocity();
        SpawnGasolina();
        SpawnMoneda();
    }

    private void ChangeVelocity()
    {
        time += Time.deltaTime;
        Controller_Enemy.enemyVelocity = Mathf.SmoothStep(1f, 15f, time / 45f);
    }

    private void SpawnEnemies()
    {
        respawningTimer -= Time.deltaTime;

        if (respawningTimer <= 0)
        {
            Instantiate(enemies[UnityEngine.Random.Range(0, enemies.Count)], instantiatePos.transform);
            respawningTimer = UnityEngine.Random.Range(2, 6);
        }
    }
    private void SpawnGasolina()
    {
        respawningTimerGasolina -= Time.deltaTime;

        if (respawningTimerGasolina <= 0)
        {
            Instantiate(Gasolina, instantiatePos.transform);
            respawningTimerGasolina = UnityEngine.Random.Range(4, 10);
            
        }
    }
    private void ChangeVelocityGasolina()
    {
        time += Time.deltaTime;
        Controller_Enemy.enemyVelocity = Mathf.SmoothStep(1f, 15f, time / 45f);
    }

    private void SpawnMoneda()
    {
        respawningTimerMoneda -= Time.deltaTime;

        if (respawningTimerMoneda <= 0)
        {
            Instantiate(Moneda, instantiatePos.transform);
            respawningTimerMoneda = UnityEngine.Random.Range(3, 6);
        }

    }

    private void ChangeVelocityMoneda()
    {
        time += Time.deltaTime;
        Controller_Enemy.enemyVelocity = Mathf.SmoothStep(1f, 15f, time / 45f);
    }
}
